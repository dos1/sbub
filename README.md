# USB-C SBUB - SBU Blocker/Breaker/Breakout/Board

![3D render of SBUB](imgs/sbub.jpg)

A 6-layer PCB with USB-C socket/plug pair, passing all signals through except SBU, which is broken out via pin headers.

Pin numbering: 1-SBU1, 2-SBU2, 3-GND. The SBU pair applies to the USB-C socket/plug closest to the pin 1 (square).

Note that some alt-modes, such as Thunderbolt or DisplayPort, require SBU connection to work. You have to connect both sides together in order to make those work through SBUB.

![Photo of five assembled SBUBs](imgs/sbubs.jpg)

First SBUBs have been successfully produced and assembled by JLCPCB using provided gerbers, BOM and placement files.
When ordering, set "Impedance Control" to "Yes", "Layer Stackup" to "JLC06161H-3313", "Remove Order Number" to "Specify a location" and "Via Covering" to "Epoxy Filled & Capped".

The design is licensed under GNU GPLv3+. This does not apply to 3D models in sbub.3dshapes directory, which were converted from EasyEDA library.

© Copyright 2024 Sebastian Krzyszkowiak <dos@dosowisko.net>
